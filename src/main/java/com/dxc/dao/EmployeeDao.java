package com.dxc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dxc.model.Employee;

@Component
public class EmployeeDao {
	
	@Autowired
	HibernateTemplate hibernateTemplate;
	
	//Add employee
	@Transactional
	public void addEmployee(Employee emp) {
		hibernateTemplate.save(emp);
	}
	
	//Get all employee
	@Transactional
	public List<Employee> getAllEmp(){
		return hibernateTemplate.loadAll(Employee.class);
	}
	
	//Get employee by ID
	@Transactional
	public Employee getEmpById(Long id) {
		return hibernateTemplate.get(Employee.class, id);
	}
	
	//Update employee 
	@Transactional
	public void updateEmp(Employee emp) {
		hibernateTemplate.update(emp);
	}
	
	//Delete employee
	@Transactional
	public void deleteEmp(Long id) {
		hibernateTemplate.delete(hibernateTemplate.load(Employee.class, id));
	}
	
}
